import React, { Component } from 'react'
import { TextInput, View, Button } from 'react-native'

export default class InputForm extends Component {
    state = {
        text: ''
    }

    onSubmit = () => {
        this.props.postDataTodo(this.state.text)
        this.setState({ text: '' })
    }

    

    render() {
        return (
            <View>
                <TextInput type="text"
                    name="title"
                    placeholder="Tambahkan aktivitas kamu disini"
                    onChangeText={(text) => this.setState({text})}
                    value={this.state.text}></TextInput>
                <Button
                    type="submit"
                    onPress={this.onSubmit}
                    title="Add" />
            </View>
        )
    }
}
