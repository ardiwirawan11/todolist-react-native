import React, { Component } from 'react'
import {View ,Text} from 'react-native'
import TodoItem from './TodoItem'

export default class TodoList extends Component {
    constructor (props){
        super(props)
    }
    render() {
        return this.props.todos.map((todo) => {
            return(
            <View key={todo._id}>
                <TodoItem
                todo={todo}
                deleteDataTodo={this.props.deleteDataTodo}
                putDataTodo={this.props.putDataTodo}
                markComplete={this.props.markComplete}
            />

            
            </View>
        )})
    }
}
