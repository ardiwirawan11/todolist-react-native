import React, { Component } from 'react'
import { Text, View, TextInput, Button} from 'react-native'

export default class Masukscreen extends Component {
    
    render() {
        return (
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text>Masuk Screen</Text>
            <TextInput placeholder="Username"></TextInput>
            <TextInput placeholder="Password"></TextInput>
            <Button
              title="Masuk"
              onPress={() => this.props.navigation.navigate('Profil')}
            />
          </View>
        );
      }
    }