import React, { Component } from 'react'
import { Text, View } from 'react-native'

export default class DaftarScreen extends Component {
    render() {
        return (
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text>Daftar Screen</Text>
            <TextInput placeholder="Username"></TextInput>
            <TextInput placeholder="Email"></TextInput>
            <TextInput placeholder="Phone Number"></TextInput>
            <TextInput placeholder="Password"></TextInput>
            <Button
              title="Daftar"
              onPress={() => this.props.navigation.navigate('Home')}
            />
          </View>
        );
      }
    }