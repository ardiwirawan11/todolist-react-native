import React, { Component } from 'react'
import {View } from 'react-native'
import Header from './Header'
import InputForm from './InputForm'
import TodoList from './TodoList'
import axios from 'axios'
const url = 'https://btm-rn.herokuapp.com/api/v1/todo'

export default class TodoHome extends Component {
    constructor(props) {
        super(props)
        this.state = {
            todos: []
        }
    }

    getData = () => {
        axios.get(url)
            .then(res => this.setState({ todos: res.data.results },console.log(res.data)))
    }
    postDataTodo = (data) => {
        axios.post(url, { title: data, isComplete: false })
        {this.getData()}
    }
    deleteDataTodo = (id) => {
        axios.delete(url + '/' + id)
        {this.getData()}
    }
    putDataTodo = (id, data) => {
        axios.put(url + '/' + id, { title: data })
        {this.getData()}
    }
    markComplete = (id) => {
        this.state.todos.map(todo => {
            if (todo._id === id) {
                axios.put(url + '/' + id, {
                    isComplete: !todo.isComplete
                })
            }
            return todo
        })
    }

    componentDidMount() {
        this.getData()
    }


    render() {
        return (
            <View>
                <Header />
                <InputForm postDataTodo={this.postDataTodo} />
                <TodoList
                    todos={this.state.todos}
                    deleteDataTodo={this.deleteDataTodo}
                    putDataTodo={this.putDataTodo}
                    markComplete={this.markComplete}
                />
            </View>
        )
    }
}
