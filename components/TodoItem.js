import React, { Component } from 'react'
import { Text, View, Button, TextInput } from 'react-native'

export default class TodoItem extends Component {
    state = {
        text: '',
        editState: false
    }

    getStyle = () => {
        return {
            padding: '10px',
            borderBottom: '1px #ccc dotted',
            textDecoration: this.props.todo.isComplete ? 'line-through' : 'none',
        }
    };


   

    editTrig = (e) => {
        e.preventDefault();
        this.setState({
            editState: true
        })
    }
    EditText() {
        return (
            <View>
                <TextInput
                    type="text"
                    name="update"
                    onChangeText={(text) => this.setState({text})}
                    value={this.state.text}></TextInput>
                <Button onPress={() => {
                    this.props.putDataTodo(this.props.todo._id, this.state.text)
                    this.setState({ editState: false })
                }} title="Update" />
            </View>
        )
    }
    render() {
        const { _id, title } = this.props.todo;
        return (
            <View>
                <Text>{title}</Text>
                <Button title="Hapus" onPress={this.props.deleteDataTodo.bind(this, _id)} style={{ float: 'right', marginRight: '10px' }}/>
                <Button title="Edit"onPress={this.editTrig} style={{ float: 'right', marginRight: '10px' }}/>
                {this.state.editState && this.EditText()}
            </View>
        )
    }
}
