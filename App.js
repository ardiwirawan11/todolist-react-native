import React from 'react';
import { View, Text, Button , TextInput} from 'react-native';
import { createAppContainer, createStackNavigator,} from 'react-navigation'; 
import TodoHome from'./components/TodoHome';
import Masukscreen from './components/Masukscreen';
import DaftarScreen from './components/DaftarScreen';
import ProfilScreen from './components/ProfilScreen';


class HomeScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Home Screen</Text>
        <Button
          title="Masuk"
          onPress={() => this.props.navigation.navigate('Masuk')}
        />
        <Button
          title="Daftar"
          onPress={() => this.props.navigation.navigate('Daftar')}
        />
        <Button
          title="Todo"
          onPress={() => this.props.navigation.navigate('Todo')}
        />
      </View>
    );
  }  
}

const AppNavigator = createStackNavigator({
  Home: {
    screen: HomeScreen,
  },
  Masuk: {
    screen: Masukscreen,
  },
  Daftar: {
    screen: DaftarScreen,
  },
  Profil: {
    screen: ProfilScreen,
  },
  Todo: {
    screen: TodoHome,
  },
});

export default createAppContainer(AppNavigator);